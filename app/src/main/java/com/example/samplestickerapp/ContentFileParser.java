/*
 * Copyright (c) WhatsApp Inc. and its affiliates.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

package com.example.samplestickerapp;

import android.content.Context;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.common.base.Strings;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ContentFileParser {

    static List<StickerPack> parseStickerPacks(@NonNull Context context) throws IOException, IllegalStateException {
        return readStickerPacks(context);
    }

    @NonNull
    private static List<StickerPack> readStickerPacks(Context context) throws IOException, IllegalStateException {
        List<StickerPack> stickerPackList = new ArrayList<>();
        String androidPlayStoreLink = "";
        String iosAppStoreLink = "";
        stickerPackList.add(readStickerPack(context));
        if (stickerPackList.size() == 0) {
            throw new IllegalStateException("sticker pack list cannot be empty");
        }
        for (StickerPack stickerPack : stickerPackList) {
            stickerPack.setAndroidPlayStoreLink(androidPlayStoreLink);
            stickerPack.setIosAppStoreLink(iosAppStoreLink);
        }
        return stickerPackList;
    }

    @NonNull
    private static StickerPack readStickerPack(Context context) throws IOException, IllegalStateException {
        String identifier = "1";
        String name = "STEGO";
        String publisher = "Group3";
        String trayImageFile = null;
        String publisherEmail = "";
        String publisherWebsite = "";
        String privacyPolicyWebsite = "";
        String licenseAgreementWebsite = "";
        String imageDataVersion = "10";
        boolean avoidCache = false;
        boolean animatedStickerPack = false;
        Log.d("myTag", Arrays.toString(context.getAssets().list("1/")));
        String[] assetArray = context.getAssets().list("1/");
        String[] imageArray = new String[assetArray.length-1];
        int n = 0;
        for (String img : assetArray) {
            if (img.endsWith(".png")) {
                trayImageFile = img;
            } else {
                imageArray[n] = img;
                n++;
            }
        }
        //imageArray[0] = "TEST.webp";
        List<Sticker> stickerList = readStickers(imageArray);
        if (TextUtils.isEmpty(identifier)) {
            throw new IllegalStateException("identifier cannot be empty");
        }
        if (TextUtils.isEmpty(name)) {
            throw new IllegalStateException("name cannot be empty");
        }
        if (TextUtils.isEmpty(publisher)) {
            throw new IllegalStateException("publisher cannot be empty");
        }
        if (TextUtils.isEmpty(trayImageFile)) {
            throw new IllegalStateException("tray_image_file cannot be empty");
        }
        if (stickerList == null || stickerList.size() == 0) {
            throw new IllegalStateException("sticker list is empty");
        }
        if (identifier.contains("..") || identifier.contains("/")) {
            throw new IllegalStateException("identifier should not contain .. or / to prevent directory traversal");
        }
        if (TextUtils.isEmpty(imageDataVersion)) {
            throw new IllegalStateException("image_data_version should not be empty");
        }
        final StickerPack stickerPack = new StickerPack(identifier, name, publisher, trayImageFile, publisherEmail, publisherWebsite, privacyPolicyWebsite, licenseAgreementWebsite, imageDataVersion, avoidCache, animatedStickerPack);
        stickerPack.setStickers(stickerList);
        return stickerPack;
    }

    @NonNull
    private static List<Sticker> readStickers(@NonNull String[] imageArray) throws IOException, IllegalStateException {

        List<Sticker> stickerList = new ArrayList<>();

        for (String imageFile : imageArray) {
            List<String> emojis = new ArrayList<>(StickerPackValidator.EMOJI_MAX_LIMIT);
            emojis.add("🙂");
            if (TextUtils.isEmpty(imageFile)) {
                throw new IllegalStateException("sticker image_file cannot be empty");
            }
            if (!imageFile.endsWith(".webp")) {
                throw new IllegalStateException("image file for stickers should be webp files, image file is: " + imageFile);
            }
            if (imageFile.contains("..") || imageFile.contains("/")) {
                throw new IllegalStateException("the file name should not contain .. or / to prevent directory traversal, image file is:" + imageFile);
            }
            stickerList.add(new Sticker(imageFile, emojis));
        }
        return stickerList;
    }
}
